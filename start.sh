#!/bin/sh

# Modfiy the docker.sock if it exists
# The reason we do this is so the local user
# can control the hosts docker socket
sudo chmod 0777 /var/run/docker.sock &>/dev/null

# Display runtime arguments for debugging
echo Arguments: $@

# Run the passed arguments in current environment
exec $@

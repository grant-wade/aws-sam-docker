# AWS and SAM CLI from Docker
The AWS and SAM CLI are both extremely useful tools for working with an AWS
environment but having them installed in a locally python environment can make
things messy. A good solution to this problem is running them their own docker
environment, created from a regular Python-alpine container. The resulting
container has docker and openrc installed to allow local running of SAM
environments, this may be changed to just `docker-cli` in the future.

## Requirements
The only requirement is a recent version of **Docker** running locally on your
machine (tested and working with MacOS Catalena & Ubuntu 20.04).

## How to Install
Installing the AWS and SAM CLI with Docker is quite easy

```
# Clone the repo to anywhere you want
git clone https://gitlab.com/grant-wade/aws-sam-docker.git

# Move to the new folder
cd aws-sam-docker

# Build the container
docker build -t aws-sam .

# Revel in its ease of use...
docker run -it --rm -v $HOME/.aws:/home/samcli/.aws $(pwd):/home/samcli aws-sam sam init
```

Okay that is probably to much work to type and run each time you need
the AWS or SAM CLI. Lets create some bash/zsh functions to make this
easier.

```bash
# ~/.bashrc, ~/.zshrc, etc

# aws: run an AWS CLI function
function aws {
	docker run -it --rm \
		-v $HOME/.aws:/home/samcli/.aws \
		-v $(pwd):/home/samcli \
		aws-sam aws $@
}

# sam: run a SAM function that doesn't require Docker
function sam {
	docker run -it --rm \
		-v $HOME/.aws:/home/samcli/.aws \
		-v $(pwd):/home/samcli \
		aws-sam sam $@	
}

# samd: run a SAM function that does require Docker
function samd {
	docker run -it --rm \
		-v $HOME/.aws:/home/samcli/.aws \
		-v $(pwd):/home/samcli \
		-v /var/run/docker.sock:/var/run/docker.sock \
		aws-sam sam $@
}


# sam-run: run an api from the local directory
function sam-run {
	docker run -it --rm \
		-v $HOME/.aws:/home/samcli/.aws \
		-v $(pwd):/home/samcli \
		-v /var/run/docker.sock:/var/run/docker.sock \
		aws-sam sam local start-app --host 0.0.0.0 \
		-v $(pwd)/.aws-sam/build
	
}
```

With those bash functions you are able to easily run the AWS and SAM CLI
from your terminal how it was intended. One note that needs to be made is
that giving any container access to your docker.sock can have security
implications, but if this is mainly used for local development and testing
of your own APIs there shouldn't be anything to worry about. 


## Usage Examples
Here are some usage examples once the bash functions are setup
```
# Initializing a project with SAM CLI

$ sam init
Which template source would you like to use?
	1 - AWS Quick Start Templates
	2 - Custom Template Location
Choice: 1

Which runtime would you like to use?
	1 - nodejs12.x
	2 - python3.8
	3 - ruby2.7
	4 - go1.x
	5 - java11
	6 - dotnetcore3.1
	7 - nodejs10.x
	8 - python3.7
	9 - python3.6
	10 - python2.7
	11 - ruby2.5
	12 - java8.al2
	13 - java8
	14 - dotnetcore2.1
Runtime: 2

Project name [sam-app]: testing

Cloning app templates from https://github.com/awslabs/aws-sam-cli-app-templates.git

AWS quick start application templates:
	1 - Hello World Example
	2 - EventBridge Hello World
	3 - EventBridge App from scratch (100+ Event Schemas)
	4 - Step Functions Sample App (Stock Trader)
	5 - Elastic File System Sample App
Template selection: 1

-----------------------
Generating application:
-----------------------
Name: testing
Runtime: python3.8
Dependency Manager: pip
Application Template: hello-world
Output Directory: .

```

```
# Example: Building testing API

$ sam build
Building function 'HelloWorldFunction'
Running PythonPipBuilder:ResolveDependencies
Running PythonPipBuilder:CopySource

Build Succeeded

Built Artifacts  : .aws-sam/build
Built Template   : .aws-sam/build/template.yaml
```


```
# Example: Using sam-run on the project

$ cd testing
$ sam-run

Mounting HelloWorldFunction at http://0.0.0.0:3000/hello [GET]
You can now browse to the above endpoints to invoke your functions. You do not need to restart/reload SAM CLI while working on your functions, changes will be reflected instantly/automatically. You only need to restart SAM CLI if you update your AWS SAM template
2020-09-10 00:10:50  * Running on http://0.0.0.0:3000/ (Press CTRL+C to quit)
```

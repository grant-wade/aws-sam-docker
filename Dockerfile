# Using recent python image
FROM python:3.8-alpine

# Install dev tools and docker for running sam locally
RUN apk add --update --no-cache dpkg-dev musl-dev gcc git sudo docker openrc && \
    pip install aws-sam-cli awscli

# Start docker instance on boot
RUN rc-update add docker boot

# Create a user and add it to the docker group
RUN adduser -D -s /bin/bash samcli docker && \
    echo 'samcli ALL=(ALL) NOPASSWD:ALL' >>/etc/sudoers

# Set the default user to samcli
USER samcli

# Port 3000 is the default local sam api
EXPOSE 3000

# Get the entrypoint script
COPY start.sh /start.sh

# Setup our workdir to the new users home directory
WORKDIR /home/samcli

# Set the entrypoint to the start script
ENTRYPOINT ["/start.sh"]
